const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')
const salt = 10
const moment = require('moment')
const uuid = require('uuid/v1')
const Boom = require('boom')

const tokenSchema = new Schema({
  token: { type: String, required: true },
  expires: { type: Date, default: moment().add(12, 'hours') }
})

const permissionsSchema = new Schema({
  itemId: { type: String, required: true },
  read: { type: Boolean, required: true },
  edit: { type: Boolean, required: true },
  create: { type: Boolean, required: true }
})

const userSchema = new Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String },
  phoneNumber: { type: String },
  avatar: { type: String, default: 'default.png' },
  fullName: { type: String, required: true },
  isAdmin: { type: Boolean, default: false },
  permissions: [permissionsSchema],
  auth: tokenSchema,
  createdOn: { type: Date, default: Date.now() }
})

let mongooseOptions = {
  autoIndex: false,
  reconnectTries: 30,
  reconnectInterval: 500,
  poolSize: 10,
  bufferMaxEntries: 0,
  useNewUrlParser: true
}

const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/stocker'

const connect = function () {
  return mongoose.createConnection(mongoURL, mongooseOptions, err => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
  })
}

module.exports = class UserDB {
  constructor () {
    this.connection = connect()
    this.users = this.connection.model('users', userSchema)
  }
  /**
   * Create User
   * @param {String} username 
   * @param {String} pass 
   * @param {String} email 
   * @param {String} phoneNumber 
   * @param {String} avatar 
   * @param {String} fullName 
   * @param {Boolean} isAdmin 
   */
  async createUser (username, pass, email, phoneNumber, fullName, isAdmin) {
    let password = await bcrypt.hash(pass, salt)
    let usercheck = await this.users.find({ $or: [ { username: username }, { email: email } ] })
    if (usercheck.length > 0) return 'Username or email already exists'
    try{
      let user = await new this.users({
        username: username,
        password: password,
        email: email,
        phoneNumber: phoneNumber,
        fullName: fullName,
        isAdmin: isAdmin,
        auth: { token: 'default', expires: moment() }
      })
      user.save()
    } catch(err) {
      return err
    }
    
    return 'User created'
  }
  /**
   * Delete User
   * @param {String} id Value can be username or email 
   * @returns {Number}
   */
  async deleteUser (id) {
    const deleted = await this.users.deleteOne({ $or: [{ username: id }, { email: id }] })
    return deleted.deletedCount
  }
  /**
   * Get all users
   * @param {String} opt Decides what fields are shown
   * @param {String} sort Decides how to sort the results
   * @returns {Object}
   */
  async getUsers (opt, sort) {
    let options = opt || 'createdOn username email phoneNumber avatar fullName isAdmin permissions'
    if(options.includes('password') || options.includes('auth')) return 'Query cannot include password or auth'
    let query = await this.users.find({}, options).sort(sort)

    return query
  }
  /**
   * Get one user
   * @param {String} id Get a user by email or username
   * @param {String} opt Decides what fields are shown
   * @returns {Object}
   */
  async getUser (id, opt) {
    let options = opt || 'createdOn username email phoneNumber avatar fullName isAdmin permissions'
    let query = await this.users.findOne({ $or: [{ username: id }, { email: id }] }, options, (err, user) => {
      return user
    })
    return query
  }
  /**
   * Generate a new token
   * @param {String} username Username of the user to generate token for
   * @returns {String}
   */
  async generateToken (username) {
    let userToken = await this.users.findOne({ username: username })
    if (!userToken) return 'User not found'
    userToken.auth.token = uuid().split('-').join('')
    userToken.auth.expires = moment().add(1, 'days')
    userToken.save()
    return userToken.auth.token
  }
  /**
   * Is the requested user an admin
   * @param {String} token The auth token
   */
  async isAdmin(token) {
    let users = await this.users.find({})
    for(var i in users) {
      if(users[i].auth.token === token) {
        if(users[i].isAdmin === true) {
          return true
        }
      }
    }
    return false
  }
  async isRequestedUser(token, username) {
    let isUser = false
    await this.users.find({ username: username }, (err, res) => {
      if (res.length < 1) return
      for (var i in res) {
        if(res[i].auth.token = token) {
          isUser = true
          return
        }
      }
    })
    return isUser
  }
  async updateAvatar (id, avatar) {
    const update = await this.users.updateOne({ $or: [{ username: id }, { _id: id }] }, { avatar: avatar }, (err)=>{if(err) return 'User not found'; else return null})
    if(update) return 'Error: user not found'
    return `Avatar for ${username} updated`
  }
  /**
   * Update User Fields
   * @param {String} username 
   * @param {String} field 
   * @param {String} value 
   */
  async updateUserFields (username, field, value) {
    let query = await this.users.findOne({ username: username })
    if(field === 'username') {query.username = value}
    if(field === 'password') {query.password = value}
    if(field === 'email') {query.email = value}
    if(field === 'phoneNumber') {query.phoneNumber = value}
    if(field === 'avatar') {query.avatar = value}
    if(field === 'fullName') {query.fullName = value}
    if(field === 'isAdmin') {query.isAdmin = value}
    if(field === 'permissions') {query.permissions = value}
    if(field === 'auth') {query.auth = value}
    query.save()
    return true
  }
  get url () {
    return process.env.MONGO_URL || 'mongodb://localhost:27017'
  }
}