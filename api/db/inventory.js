const mongoose = require('mongoose')
const Schema = mongoose.Schema
const moment = require('moment')

const locationsSchema = new Schema({
  location: { type: String },
  phone: { type: String },
  email: { type: String }
})

const vendorsSchema = new Schema({
  name: { type: String },
  locations: [locationsSchema],
  url: { type: String },
  items: [String]
})

const historySchema = new Schema({
  user: { type: String },
  changed: { type: String },
  quantity: { type: Number },
  // price: { type: Number },
  changedOn: { type: Date, default: moment() }
})

const itemSchema = new Schema({
  prodName: { type: String, required: true },
  brand: { type: String },
  category: { type: String },
  modelNumber: { type: String },
  quantity: { type: Number, min: 0, required: true },
  required: { type: Number, min: 1, required: true },
  type: { type: String },
  location: { type: String },
  storageLocation: { type: String },
  icon: { type: String },
  UPC: { type: String },
  vendors: [String],
  manual: { type: String },
  SDS: { type: String },
  specs: [String],
  notes: { type: String },
  history: [historySchema],
  createdOn: { type: Date, default: moment() }
})

let mongooseOptions = {
  autoIndex: false,
  reconnectTries: 30,
  reconnectInterval: 500,
  poolSize: 10,
  bufferMaxEntries: 0,
  useNewUrlParser: true
}

const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/stocker'

const connect = function () {
  return mongoose.createConnection(mongoURL, mongooseOptions, err => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
  })
}

module.exports = class UserDB {
  constructor () {
    this.connection = connect()
    this.items = this.connection.model('items', itemSchema)
    this.vendors = this.connection.model('vendors', vendorsSchema)
  }
  async createItem (item) {
    const newItem = await new this.items(item)
    await newItem.save()
    return 'Item created'
  }
  async deleteItem (item) {
    const deleted = await this.items.deleteOne({ _id: item })
    return deleted.deletedCount
  }
  async getItems (opt, sort) {
    let query = await this.items.find({}, opt).sort(sort)
    return query
  }
  async updateItem(item, field, value) {
    let query = await this.items.findById(item)
    if(field === 'prodName') {query.prodName = value}
    if(field === 'brand') {query.brand = value}
    if(field === 'category') {query.category = value}
    if(field === 'modelNumber') {query.modelNumber = value}
    if(field === 'type') {query.type = value}
    if(field === 'location') {query.location = value}
    if(field === 'required') {query.required = value}
    if(field === 'storageLocation') {query.storageLocation = value}
    if(field === 'icon') {query.icon = value}
    if(field === 'UPC') {query.UPC = value}
    if(field === 'vendors') {query.vendors = value}
    if(field === 'manual') {query.manual = value}
    if(field === 'SDS') {query.SDS = value}
    if(field === 'specs') {query.specs = value}
    if(field === 'notes') {query.notes = value}
    if(!query) return 'There was a problem updating'
    query.save()
    return 'Item updated'
  }
  async getItem (item, sort) {
    let query = await this.items.findById(item, sort)
    return query
  }
  async updateHistory(item, history) {
    let query = await this.items.findById(item)
    query.history.push(history)
  }
  async getHistory () {
    let query = await this.items.find()
    let history = []
    for(var item in query) {
      for(var i = 0; i < query[item].history.length; i++) {
        history.push( {
          prodName: query[item].prodName,
          user: query[item].history[i].user,
          changed: query[item].history[i].changed,
          quantity: query[item].history[i].quantity,
          changedOn: query[item].history[i].changedOn
        } )
      }
    }
    return history
  }
}