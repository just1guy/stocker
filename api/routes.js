const hAuth = require('./auth/hapi-validate')
const Boom = require('boom')

module.exports = (server) => {

  const browserRoutes = [
    '/{file?}',
    '/about/{file?}',
    '/help/{file?}',
    '/history/{file?}',
    '/home/{file?}',
    '/inventory/{file?}',
    '/settings/{file?}',
    '/stats/{file?}',
    '/vendors/{file?}',
    '/checkout/{file?}'
  ]

  for(var i in browserRoutes) {
    server.route({
      method: 'GET',
      path: browserRoutes[i],
      options: {
        auth: {
          mode: 'try',
          strategies: ['strong']
        },
      },
      handler: async (request, h) => {
        if(!request.auth.isAuthenticated) return h.redirect('/login')
        if(String(request.params.file) === 'undefined') return h.file('./dist/index.html')
        return h.file(`./dist/${request.params.file}`)
      }
    })
  }

  server.route({
    method: 'GET',
    path: '/login',
    options: {
      auth: false
    },
    handler: async (request, h) => {
      let check = await hAuth.validateToken(request.yar.get('token'))
      if(check.isValid) {
        request.yar.set('token', request.auth.credentials.token)
        return h.redirect('/')
      }
      return h.file('./login.html')
    }
  })

  server.route({
    method: 'GET',
    path: '/logout',
    handler: (request, h) => {
      request.yar.set('token', null)
      return h.redirect('/login')
    }
  })

  server.route({
    method: 'POST',
    path: '/login',
    options: {
      auth: false
    },
    handler: async (request, h) => {
      const { username, password } = request.payload
      const validate = await hAuth.validateUsername(request, username, password)
      if(validate.isValid) {
        await request.yar.set('token', validate.credentials.token)
        return h.redirect(`/`)
      }
      return Boom.unauthorized('Invalid username or password')
    }
  })

  server.route({
    method: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE'],
    path: '/api',
    handler: (request, h) => 'api page'
  })

  server.route({
    method: 'GET',
    path: '/api/version',
    options: {
      cors: true,
      auth: false,
    },
    handler: async (request, h) => {
      return { version: require('../package.json').version }
    }
  })

}