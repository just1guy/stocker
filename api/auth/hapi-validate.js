const bcrypt = require('bcrypt')
const UserDB = require('../db/users')
const moment = require('moment')

const userdb = new UserDB()

exports.validateUsername = async (request, username, password) => {
  const user = await userdb.getUser(username, {username: 1, auth: 1, password: 1})
  if (!user) return { isValid: false }

  const isValid = await bcrypt.compare(password, user.password)
  if (!isValid) return { isValid }
  let newToken = null
  if(user.auth.expires < moment()) {
    newToken = userdb.generateToken(username)
  } else {
    // userdb.updateTokenExpiry()
    user.auth.expires = moment().add(1, 'days')
    user.save()
    newToken = user.auth.token
  }
  const credentials = { name: username, token: newToken }

  return { isValid, credentials }
}

exports.validateToken = async (token) => {
  if(!token) return { isValid: false, credentials: null }
  let getToken
  let isAdmin
  let user
  let users = await userdb.users.find({})
  for (var i = 0; i < users.length; i++) {
    if(users[i].auth.token === token) {
      if(users[i].auth.expires < moment()) return { isValid: false, credentials: null }
      isAdmin = users[i].isAdmin
      user = users[i].username
      getToken = users[i].auth.token
      users[i].save()
      break
    }
  }
  if (!getToken) return { isValid: false, credentials: null }
  const isValid = true
  const credentials = { token: getToken, isAdmin: isAdmin, user: user }

  return { isValid, credentials }
}
