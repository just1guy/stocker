const jimp = require('jimp')

exports.makeAvatar = async function (buffer) {
  const image = await jimp.read(buffer)
  const res = await image.resize(128, 128).getBufferAsync(jimp.MIME_PNG)
  return res
}
