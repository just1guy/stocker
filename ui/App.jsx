import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import Inventory from './components/pages/Inventory'
import Vendors from './components/pages/Vendors'
import History from './components/pages/History'
import Stats from './components/pages/Stats'
import Settings from './components/pages/Settings'
import Help from './components/pages/Help'
import CheckOut from './components/pages/CheckOut'
import About from './components/pages/About'
import io from 'socket.io-client'
const socket = io(`${window.location.protocol}//${window.location.host}`)

import ons from 'onsenui'
import * as Ons from 'react-onsenui'
import './css/bootstrap/bootstrap-stocker.scss'
import './css/main.scss'
import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'
import Dialog from './components/Dialog';

if(ons.platform.isIOS() || 
  ons.platform.isIOS7above() || 
  ons.platform.isIOSSafari() || 
  ons.platform.isIPad() || 
  ons.platform.isIPhone() || 
  ons.platform.isIPhoneX() || 
  ons.platform.isSafari() || 
  ons.platform.isUIWebView()) ons.platform.select('ios')
else ons.platform.select('android')

console.log(`${ons.platform.isAndroid()}\n${ons.platform.isIOS()}`)

const ControlPanel = (props) => {
  const [saveRes, setSaveRes] = useState(false)
  const createItem = () => {
    const update = [
      document.getElementById('create-product'),
      document.getElementById('create-brand'),
      document.getElementById('create-category'),
      document.getElementById('create-type'),
      document.getElementById('create-model'),
      document.getElementById('create-required'),
      document.getElementById('create-quantity'),
      document.getElementById('create-location'),
      document.getElementById('create-storage'),
      document.getElementById('create-notes')
    ]
    let xhr = new XMLHttpRequest()
    let res = {
      get: null,
      set: v => res.get = v
    }
    xhr.open('POST', `/api/items`)
    xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
    xhr.onreadystatechange = () => {
      if(xhr.readyState === 4) res.set(xhr.status)
    }
    xhr.send(JSON.stringify({
      item: {
        prodName: update[0].value,
        brand: update[1].value,
        category: update[2].value,
        type: update[3].value,
        modelNumber: update[4].value,
        required: update[5].value,
        quantity: update[6].value,
        location: update[7].value,
        storageLocation: update[8].value,
        notes: update[9].value,
      }
    }))
    let wait = setInterval(() => {
      if(res.get === null) return
      if(res.get === 200) {
        clearInterval(wait)
        for(var entry in update) {
          update[entry][1].value = ''
        }
        props.setcpShown(false)
        return
      }
      console.log(res.get)
      setSaveRes(true)
      setTimeout(() => setSaveRes(false), 3000)
      clearInterval(wait)
    }, 200)
  }
  const saveItem = (id) => {
    const update = [
      ['prodName', document.getElementById('edit-product')],
      ['brand', document.getElementById('edit-brand')],
      ['category', document.getElementById('edit-category')],
      ['type', document.getElementById('edit-type')],
      ['modelNumber', document.getElementById('edit-model')],
      ['required', document.getElementById('edit-required')],
      ['location', document.getElementById('edit-location')],
      ['storageLocation', document.getElementById('edit-storage')],
      ['notes', document.getElementById('edit-notes')]
    ]
    let list = []
    for(var entry in update) {
      if(update[entry][1].value == '') continue
      list.push([ update[entry][0], update[entry][1].value ])
    }
    let xhr = new XMLHttpRequest()
    let res = {
      get: null,
      set: v => res.get = v
    }
    xhr.open('POST', `/api/items/${id}`)
    xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
    xhr.onreadystatechange = () => {
      if(xhr.readyState === 4) res.set(xhr.status)
    }
    xhr.send(JSON.stringify(list))
    let wait = setInterval(() => {
      if(res.get === null) return
      if(res.get === 200) {
        clearInterval(wait)
        for(var entry in update) {
          update[entry][1].value = ''
        }
        props.setcpShown(false)
        return
      }
      console.log(res.get)
      setSaveRes(true)
      setTimeout(() => setSaveRes(false), 3000)
      clearInterval(wait)
    }, 1000)
  }
  switch (props.data.action) {
    case 'Create Item':
    return (
      <div style={{padding: '1rem', backgroundColor: '#fff'}}>
        <Ons.Row>
          Product
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-product'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Brand
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-brand'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Category
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-category'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Type
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-type'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Model
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-model'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Quantity
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input type='number' min={1} className='w-100' id='create-quantity'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Required
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input type='number' min={1} className='w-100' id='create-required'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Location
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-location'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Storage
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-storage'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Notes
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='create-notes'/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          <div className='d-flex justify-content-between w-100'>
            <Ons.Button className='bg-secondary' onClick={()=>props.setcpShown(false)}>Cancel</Ons.Button>
            <Ons.Button onClick={() => createItem()}>Save</Ons.Button>
          </div>
        </Ons.Row>
        <Ons.Toast isOpen={saveRes}>
          <span className='text-danger'>Warning: An error occurred while creating the item</span>
        </Ons.Toast>
      </div>
    )
    case 'Edit Item':
    return (
      <div style={{padding: '1rem', backgroundColor: '#fff'}}>
        <Ons.Row>
          Product
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-product' placeholder={props.data.data.prodName}/>
        </Ons.Row>
        <Ons.Row>
          Brand
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-brand' placeholder={props.data.data.brand}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Category
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-category' placeholder={props.data.data.category}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Type
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-type' placeholder={props.data.data.type}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Model
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-model' placeholder={props.data.data.modelNumber}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Required
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input type='number' min={1} className='w-100' id='edit-required' placeholder={props.data.data.required}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Location
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-location' placeholder={props.data.data.location.substr(0,25) + '...'}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Storage
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-storage' placeholder={props.data.data.storageLocation}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          Notes
        </Ons.Row>
        <Ons.Row style={{paddingTop: '0.5rem'}}>
          <Ons.Input className='w-100' id='edit-notes' placeholder={props.data.data.notes}/>
        </Ons.Row>
        <Ons.Row style={{paddingTop: '1rem'}}>
          <div className='d-flex justify-content-between w-100'>
            <Ons.Button className='bg-secondary' onClick={()=>props.setcpShown(false)}>Cancel</Ons.Button>
            <Ons.Button onClick={() => saveItem(props.data.data._id)}>Save</Ons.Button>
          </div>
        </Ons.Row>
        <Ons.Toast isOpen={saveRes}>
          <span className='text-danger'>Warning: An error occurred while updating the item</span>
        </Ons.Toast>
      </div>
    )
  }
  return null
}

const App = () => {
  const [spShown, setspShown] = useState(false)
  const [cpShown, setcpShown] = useState(false)
  const [cpData, setcpData] = useState({
    data: undefined,
    action: ''
  })
  const [dShown, setdShown] = useState(false)
  const [dData, setdData] = useState({
    title: '',
    content: '',
    action: () => {},
    btnClass: '',
    btnText: ''
  })
  const [currentUser, setCurrentUser] = useState({})
  const [items, setItems] = useState([])
  const [users, setUsers] = useState([])
  const [history, setHistory] = useState([])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    socket.on('history', message => {
      let xhr = new XMLHttpRequest()
      xhr.open('GET', `/api/history`)
      xhr.onreadystatechange = () => {
        try {
          let history = JSON.parse(xhr.response)
          setHistory(history)
        } catch(err) {
        }
      }
      xhr.send()
    } )
    return () => socket.off('history')
  })

  useEffect(() => {
    socket.on('users', message => {
      let xhr = new XMLHttpRequest()
      xhr.open('GET', `/api/users`)
      xhr.onreadystatechange = () => {
        try {
          let users = JSON.parse(xhr.response)
          setUsers(users)
        } catch(err) {
        }
      }
      xhr.send()
    } )
    return () => socket.off('users')
  })

  useEffect(() => {
    let xhr = new XMLHttpRequest()
    xhr.open('GET', `/api/users/current`)
    xhr.onreadystatechange = () => {
      try {
        let user = JSON.parse(xhr.response)
        setCurrentUser({
          username: user.username,
          email: user.email,
          fullName: user.fullName,
          isAdmin: user.isAdmin,
          permissions: user.permissions,
          phoneNumber: user.phoneNumber
        })
      } catch(err) {
      }
    }
    xhr.send()
  }, [])

  useEffect(() => {
    socket.on('refresh', message => {
      let xhr = new XMLHttpRequest()
      xhr.open('GET', `/api/items`)
      xhr.onreadystatechange = () => {
        try {
          let items = JSON.parse(xhr.response)
          setItems(items)
          setLoading(false)
        } catch(err) {
        }
      }
      xhr.send()
    } )
    return () => socket.off('refresh')
  })

  
  

  return (
    <Router>
      <Ons.Splitter>
        <Ons.SplitterSide
          side='left'
          width={280}
          swipeable={true}
          isOpen={spShown}
          collapse
          onClose={()=>setspShown(false)}
          animation='reveal'>
          <Ons.Page renderToolbar={() => (
            <Ons.Toolbar style={{backgroundColor: '#2e4052'}}>
              <div className='center text-light'>
                Main Menu
              </div>
            </Ons.Toolbar>
          )}
          renderBottomToolbar={() => (
            <Ons.BottomToolbar style={{justifyContent: 'center', alignItems: 'center', display: 'flex'}} >
              <Ons.Button modifier='outline large' style={{cursor: 'pointer'}} onClick={() => window.open('/logout', '_self')}>
                Sign Out
              </Ons.Button>
            </Ons.BottomToolbar>
          )}>
            <Ons.List 
              dataSource={[
                ['Inventory', 'md-view-list', '/'], 
                ['Check Out', 'md-undo', '/checkout'],
                ['History', 'md-calendar', '/history'], 
                ['Settings', 'md-settings', '/settings'], 
                ['About', 'md-help', '/about']]}
              renderRow={(row, i) => (
                <Link key={i} to={row[2]} onClick={() => setspShown(false)}>
                  <Ons.ListItem>
                    <div className='left'>
                      <Ons.Icon icon={row[1]} />
                    </div>
                    <div className='center'>
                      {row[0]}
                    </div>
                  </Ons.ListItem>
                </Link>
              )}
            />
          </Ons.Page>
        </Ons.SplitterSide>
        <Ons.SplitterContent>

          <Ons.Page renderToolbar={() => (
            <Ons.Toolbar style={{backgroundColor: '#2e294e'}}>
              <div className='left'>
                <Ons.ToolbarButton className='text-light'
                  onClick={()=>setspShown(true)}>
                  <Ons.Icon icon='md-menu' />
                </Ons.ToolbarButton>
              </div>
              <div className='center text-light'>
                Stocker
              </div>
              <div className='right'>
              </div>
            </Ons.Toolbar>
            )}>

            <Route exact path='/' render={() => (
              <Inventory 
                items={items}
                currentUser={currentUser}
                setcpData={setcpData}
                setcpShown={setcpShown}
                setdData={setdData}
                setdShown={setdShown}
              />
            )} />
            <Route path='/about' render={() => (
              <About />
            )}/>
            <Route path='/checkout' render={() => (
              <CheckOut />
            )}/>
            <Route path='/history' render={() => (
              <History 
                history={history}
                items={items}
              />
            )}/>
            <Route path='/settings' render={() => (
              <Settings 
                currentUser={currentUser}
                users={users}
                setdData={setdData}
                setdShown={setdShown}
              />
            )}/>

            </Ons.Page>

        </Ons.SplitterContent>
        <Ons.SplitterSide
          side='right'
          width={280}
          isOpen={cpShown}
          collapse
          onClose={()=>setcpShown(false)}
          animation='reveal'>
          <Ons.Page
            renderToolbar={() => (
              <Ons.Toolbar>
                <div className='left'><Ons.BackButton onClick={() => {setcpShown(false)}}>Back</Ons.BackButton></div>
                <div className='center'>{cpData.action}</div>
                <div className='right'></div>
              </Ons.Toolbar>
            )}>
            <ControlPanel 
              data={cpData}
              setcpShown={setcpShown}
            />
          </Ons.Page>
        </Ons.SplitterSide>
        <Ons.Modal
          animation='fade' 
          isOpen={loading}>
          <Ons.Icon icon='md-spinner' spin={true} />&nbsp;Loading...
        </Ons.Modal>
        <Ons.AlertDialog isOpen={dShown} onCancel={() => setdShown(false)}>
          <div className='alert-dialog-title'>{dData.title}</div>
          <div className='alert-dialog-content'>{dData.content}</div>
          <div className='alert-dialog-footer d-flex justify-content-between w-100 px-4 mb-4 mt-2'>
            <Ons.Button onClick={() => setdShown(false)} className='bg-secondary'>Cancel</Ons.Button>
            <Ons.Button onClick={dData.action} className={dData.btnClass}>{dData.btnText}</Ons.Button>
          </div>
        </Ons.AlertDialog>

      </Ons.Splitter>
    </Router>
  )
}

export default hot(module)(App)