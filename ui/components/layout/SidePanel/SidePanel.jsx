import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import options from './SidePanelOptions.jsx'

const SidePanel = (props) => {
  
  const renderSidePanel = (props) => {
    return options.map(item => {
      if (item.divider === true) return (<div key={item._id} className="sp-divider"></div>)
      return (
        <Link key={item._id} className="sp-item align-items-center" to={item.route} onClick={() => props.closeSidePanel()}>

          <div className="sp-icon">
            <i className='material-icons'>{item.icon}</i>
          </div>

          <div className="sp-text">
            <span>{item.text}</span>
          </div>

        </Link>
      )
    })
  }

  return (
    <div className={props.spShown + ' noselect d-print-none'}>
      <div className={props.spClass}>

        <div className='py-2 d-flex sidepanel-top bg-charcoal align-items-center'>
          <i className='material-icons header-button pl-4' onClick={() => props.closeSidePanel()}>menu</i>
          <span className='pl-4'>Stocker Server</span>
        </div>

        {renderSidePanel(props)}
        
      </div>
      <div className={props.spBClass} onClick={() => props.closeSidePanel()} />
    </div>
  )
}

export default SidePanel