export default [
    { 
        _id: 1, 
        icon: "home", 
        text: "Home",
        route: "/"
    },
    { 
        _id: 2, 
        icon: "list", 
        text: "Inventory",
        route: "/inventory"
    },
    { 
        _id: 3, 
        icon: "shopping_cart", 
        text: "Vendors",
        route: "/vendors"
    },
    { 
        _id: 4, 
        divider: true 
    },
    { 
        _id: 5, 
        icon: "history", 
        text: "History",
        route: "/history"
    },
    { 
        _id: 6, 
        icon: "assessment", 
        text: "Stats",
        route: "/stats"
    },
    { 
        _id: 7, 
        divider: true 
    },
    { 
        _id: 8, 
        icon: "settings", 
        text: "Settings",
        route: "/settings"
    },
    { 
        _id: 9, 
        icon: "help", 
        text: "Help",
        route: "/help"
    },
    { 
        _id: 10, 
        icon: "help_outline", 
        text: "About",
        route: "/about"
    }
]
