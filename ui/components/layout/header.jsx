import React, { useState } from 'react'
import { OverlayTrigger, Popover, Button } from 'react-bootstrap'



const Header = (props) => {
  return (
    <div className='d-flex d-print-none justify-content-between bg-space px-2 py-1 align-items-center noselect' id='header'>
      <i onClick={() => { props.openSidePanel() }} className='pl-3 pr-4 material-icons header-button'>menu</i>
      <img src='/img/stocker.svg' height='40' />
      {/* <div className='searchbox d-none d-md-flex align-items-center'>
        <i className='material-icons px-3'>search</i>
        <input className="flex-fill mr-3" id="search-input" placeholder="Search Inventory" type='text'/>
      </div> */}
      <div className='d-flex align-items-center justify-content-end'>
        <i className='material-icons header-button pr-3 pl-4'>notifications</i>
        <i className='material-icons header-button pr-3' onClick={() => { props.showAcct() }}>face</i>
      </div>
    </div>
  )
}

export default Header