import React, {useState, useEffect} from 'react'
import * as Ons from 'react-onsenui'
import { isUndefined } from 'util';

const DialogBottom = (props) => {
  switch(props.data.action) {
    case 'Edit Item':
      return (
        <React.Fragment>
          <Ons.Row>
            <Ons.Button 
              onClick={() => props.setShown(false)}>
              Cancel              
            </Ons.Button>
          </Ons.Row>
          <Ons.Row>
            <Ons.Button
              className='bg-success'
              onClick={() => {
                props.editItem(props.data._id, props.data.field, props.input)
              }}>
              Save
            </Ons.Button>
          </Ons.Row>
        </React.Fragment>
      )
  }
  return null
}

const DialogBody = (props) => {
  switch(props.data.action) {
    case 'Edit Item':
      return (
        <React.Fragment>
          <Ons.Row>
            {props.data.field}
          </Ons.Row>
          <Ons.Row>
            <Ons.Input type='text' value={props.input} onChange={(e) => props.setInput(e.target.value)} />
          </Ons.Row>
          <Ons.Row className={props.valid ? 'd-none' : ''}>
            <small className='text-danger'>A problem occured during update</small>
          </Ons.Row>
        </React.Fragment>
      )
  }
  return null
}

const Dialog = (props) => {
  const [input, setInput] = useState(isUndefined(props.data.value) ? '' : props.data.value)
  const [valid, setValid] = useState(true)
  const editItem = (id, field, value) => {
    if(props.data.value === input) return false
    let xhr = new XMLHttpRequest()
    let result = {
      get: false,
      set: v => result.get = v
    }
    xhr.open('POST', `/api/items/${id}`)
    xhr.onreadystatechange = () => {
      if(xhr.status != 200) setValid(false)
      else {
        setValid(true)
        props.setShown(false)
      }
    }
    xhr.send(`{ ${field}: ${value} }`)
  }

  return (
    <Ons.AlertDialog
      isOpen={props.shown}
      onCancel={() => {
        setValid(true)
        props.setShown(false)
      }}
      isCancelable={true}>
      <Ons.Page
        style={{height: 300}}>
        <DialogBody 
          action={props.action}
          data={props.data}
          input={input}
          setInput={setInput}
          valid={valid}
          setValid={setValid}
        />
        <DialogBottom 
            editItem={editItem}
            input={input}
            data={props.data}
            action={props.action}
            setShown={props.setShown}/>
      </Ons.Page>
    </Ons.AlertDialog>
  )
}

export default Dialog