import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Home = (props) => {
  return (
    <Container className='mt-4'>
      <Row>
        <Col>
          <Card>
            <Card.Header>Inventory</Card.Header>
            <Card.Body>
              <Card.Title>Gibe de inventory b0ss</Card.Title>
              <Card.Text>
                Home page under construction
              </Card.Text>
              <Link className='btn btn-outline-primary' to='/inventory'>Go to inventory page</Link>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default Home
