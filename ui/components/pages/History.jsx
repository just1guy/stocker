import React, { useEffect, useState } from 'react'
import { Row, Col, Form, Table, Button, Container } from 'react-bootstrap'
import * as Ons from 'react-onsenui'
import moment from 'moment'

const HistoryList = (props) => {
  return ( 
    <tr key={props.history._id}>
      <td><img className='avatar d-inline d-md-none' src={`/api/users/${props.history.user}/avatar`} height='36' title={props.history.user} /><span className='d-none d-md-inline'>{props.history.user}</span></td>
      <td className='d-none d-md-table-cell'>{props.history.changed}</td>
      <td><span className='d-inline d-md-none'>{props.history.changed === 'Removed' ? '-' : '+'}</span>{props.history.quantity}</td>
      <td>{moment(props.history.changedOn).format('l h:mm a')}</td>
    </tr>
  )
}

const History = (props) => {
  const [filter, setFilter] = useState('')
  const [sort, setSort] = useState('date')

  const gen = (item) => {
    return item.history.sort((a, b) => {
      return a.changedOn < b.changedOn ? 1 : -1
    }).map(entry => (
      <HistoryList 
        history={{ 
          user: entry.user,
          changed: entry.changed,
          quantity: entry.quantity,
          changedOn: entry.changedOn,
          prodName: item.prodName,
          _id: entry._id
        }}
      />
    ))
  }

  const renderHistory = () => {
    if(sort === 'name') {
      return props.items.sort((a, b) => {
        return a.prodName < b.prodName ? -1 : 1
      }).filter(item => {
        if(item.history.length === 0) return false
        return item.prodName.toLowerCase().includes(filter.toLowerCase())
      }).map(item => {
        return (
          <React.Fragment>
            <tr className='bg-light'>
              <td colSpan='4'>{item.prodName}</td>
            </tr>
            {gen(item)}
          </React.Fragment>
        )
      })
    }
    return props.history.sort((a, b) => {
      let res = a.changedOn < b.changedOn ? 1 : -1
      return res
    }).filter(item => {
      return item.prodName.toLowerCase().includes(filter.toLowerCase())
    }).map(item => {
      return (
        <tr>
          <td>
            <img className='avatar' src={`/api/users/${item.user}/avatar`} height='36' title={item.user} />
            <span style={{width: '1rem'}} className='d-none d-md-inline'>&nbsp;</span>
            <span className='d-none d-md-inline'>{item.user}</span>
          </td>
          <td>{item.prodName}</td>
          <td className='d-none d-md-table-cell'>{item.changed}</td>
          <td><span className='d-inline d-md-none'>{item.changed === 'Removed' ? '-' : '+'}</span>{item.quantity}</td>
          <td>{moment(item.changedOn).format('l h:mm a')}</td>
        </tr>
      )
    })
  }

  const RenderHead = () => {
    if(sort === 'name') {
      return (
        <tr>
          <th>User</th>
          <th className='d-none d-md-table-cell'>Action</th>
          <th>Q<span className='d-none d-md-inline'>uanti</span>ty</th>
          <th>Date</th>
        </tr>
      )
    }
    return (
      <tr>
        <th>User</th>
        <th>Product</th>
        <th className='d-none d-md-table-cell'>Action</th>
        <th>Q<span className='d-none d-md-inline'>uanti</span>ty</th>
        <th>Date</th>
      </tr>
    )
  }

  return (
    <Ons.Page>
      <div className='m-4'>
        <Ons.Row>
          <Ons.Col className='pb-2'>
            Filter
          </Ons.Col>
        </Ons.Row>
        <Ons.Row className='pb-2'>
          <Ons.Col>
            <Ons.SearchInput className='w-100' type="text" placeholder="Filter" onChange={(e) => {setFilter(e.target.value)}} />
          </Ons.Col>
        </Ons.Row>
        <Ons.Row>
          <Ons.Col className='pb-2'>
            Sort By
          </Ons.Col>
        </Ons.Row>
        <Ons.Row>
          <Ons.Col className='d-flex align-items-center'>
            <Ons.Button modifier='large' onClick={() => {
              setSort(sort === 'date' ? 'name' : 'date')
            }}>{sort === 'date' ? 'Date' : 'Product Name'}</Ons.Button>
          </Ons.Col>
        </Ons.Row>
        <Table bordered hover className='mt-4' size='sm'>
          <thead className='bg-light'>
            <RenderHead />
          </thead>
          <tbody className='bg-white'>
            {renderHistory()}
          </tbody>
        </Table>
      </div>
    </Ons.Page>
  )
}

export default History