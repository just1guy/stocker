import React, { useState } from 'react'
import {Container, Row, Col, Form, FormGroup, Button, Table} from 'react-bootstrap'
import * as Ons from 'react-onsenui'
import ons from 'onsenui'

const UserItemMenu = (props) => {
  const renderControl = () => {
    if(props.currentUser.isAdmin) return (
      <Ons.Row className='pt-2'>
        <Ons.Col className='d-flex justify-content-center w-100'>
          <Ons.Button className='bg-danger' onClick={() => {
            props.setdData({
              title: `Delete ${props.user.fullName}`,
              content: 'Are you sure you want to delete this user?',
              btnText: 'Delete',
              btnClass: 'bg-danger',
              action: () => {
                let xhr = new XMLHttpRequest()
                xhr.open('DELETE', '/api/users')
                xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
                xhr.send(JSON.stringify({ id: props.user._id }))
                props.setdShown(false)
              }
            })
            props.setdShown(true)
          }}>Delete User</Ons.Button>
        </Ons.Col>
      </Ons.Row>
    )
    return null
  }
  return (
    <tr className='bg-light' key={props.user._id}>
      <td colSpan={4}>
        <Container>
          <Ons.Row>
            <Ons.Col>
              <Ons.Row>
                <Ons.Col className='border-bottom'>Email: {props.user.email}</Ons.Col>
              </Ons.Row>
              <Ons.Row>
                <Ons.Col className='border-bottom'>Phone: {props.user.phoneNumber}</Ons.Col>
              </Ons.Row>
              <Ons.Row>
                <Ons.Col className='border-bottom'>Admin: {String(props.user.isAdmin)}</Ons.Col>
              </Ons.Row>
              {renderControl()}
            </Ons.Col>
          </Ons.Row>
        </Container>
      </td>
    </tr>
  )
}

const UserItem = (props) => {
  const [shown, setShown] = useState(false)

  const isShown = () => {
    if(shown) return (
      <UserItemMenu 
        user={props.user} 
        currentUser={props.currentUser}
        setdData={props.setdData}
        setdShown={props.setdShown}
      />
    )
    return null
  }
  return (
    <React.Fragment key={props.user._id}>
      <tr onClick={() => setShown(!shown)} className='hover-cursor'>
        <td><img src={`/api/users/${props.user.username}/avatar`} className='avatar' width={36} /></td>
        <td>{props.user.username}</td>
        <td className='d-none d-md-table-cell'>{props.user.email}</td>
        <td>{props.user.fullName}</td>
      </tr>
      {isShown()}
    </React.Fragment>
  )
}

const Settings = (props) => {

  const UsersList = () => {
    
    return props.users.map((user) => (
      <UserItem 
        user={user} 
        currentUser={props.currentUser}
        setdData={props.setdData}
        setdShown={props.setdShown}
      />
    ))
  }

  const CreateUser = () => {
    const [newUserInvalid, setNewUserInvalid] = useState('d-none')
    if(!props.currentUser.isAdmin) return null
    return (
      <React.Fragment>
        <Ons.Row>
          <Ons.Col className='pl-4'>
            <h2>Create User</h2>
          </Ons.Col>
        </Ons.Row>
        <Ons.Row className='bg-light p-4'>
          <Ons.Col>
            <Form onSubmit={(e) => {
                e.preventDefault()
                let username = document.getElementById('new-user-username-input')
                let password = document.getElementById('new-user-password-input')
                let email = document.getElementById('new-user-email-input')
                let phone = document.getElementById('new-user-phone-input')
                let fullname = document.getElementById('new-user-fullname-input')
                let isAdmin = document.getElementById('new-user-isadmin-input')

                console.log(isAdmin.checked)
                let finishedForms = {
                  get: false,
                  set: v => finishedForms.get = v
                }

                let newUser = new XMLHttpRequest()
                newUser.open('POST', '/api/users')
                newUser.onreadystatechange = () => {
                  if(newUser.status === 200) finishedForms.set(true)
                  else setNewUserInvalid('')
                }
                newUser.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
                newUser.send(JSON.stringify({
                  username: username.value,
                  password: password.value,
                  email: email.value,
                  phonenumber: phone.value,
                  fullname: fullname.value,
                  isadmin: isAdmin.checked
                }))

                let checker = setInterval(() => {
                  if(finishedForms.get) window.open('/settings', '_self')
                }, 200)

                setTimeout(() => {
                  clearInterval(checker)
                }, 5000)

              }}>
              <Ons.Row>
                Username
              </Ons.Row>
              <Ons.Row style={{paddingTop: '0.5rem'}}>
                <Ons.Input modifier='underbar' className='w-100' id='new-user-username-input' type="text" />
                <span className={'text-danger ' + newUserInvalid}>Username or email is already taken</span>
              </Ons.Row>
              <Ons.Row style={{paddingTop: '1rem'}}>
                Password
              </Ons.Row>
              <Ons.Row style={{paddingTop: '0.5rem'}}>
                <Ons.Input modifier='underbar' className='w-100' id='new-user-password-input' type="password" />
              </Ons.Row>
              <Ons.Row style={{paddingTop: '1rem'}}>
                Email
              </Ons.Row>
              <Ons.Row style={{paddingTop: '0.5rem'}}>
                <Ons.Input modifier='underbar' className='w-100' id='new-user-email-input' type="email" />
              </Ons.Row>
              <Ons.Row style={{paddingTop: '1rem'}}>
                Phone Number
              </Ons.Row>
              <Ons.Row style={{paddingTop: '0.5rem'}}>
                <Ons.Input modifier='underbar' className='w-100' id='new-user-phone-input' type="text" />
              </Ons.Row>
              <Ons.Row style={{paddingTop: '1rem'}}>
                Full Name
              </Ons.Row>
              <Ons.Row style={{paddingTop: '0.5rem'}}>
                <Ons.Input modifier='underbar' className='w-100' id='new-user-fullname-input' type="text" />
              </Ons.Row>
              <Ons.Row style={{paddingTop: '1rem'}}>
                Is Admin
              </Ons.Row>
              <Ons.Row style={{paddingTop: '0.5rem'}}>
                <Ons.Checkbox id="new-user-isadmin-input" />
              </Ons.Row>
              <Ons.Button type='submit' style={{marginTop: '1rem'}}><Ons.Icon icon='md-save' />&nbsp;Save</Ons.Button>
            </Form>
          </Ons.Col>
        </Ons.Row>
      </React.Fragment>
    )
  }

  const UserMenuButtons = (props) => {
    if(props.currentUser.isAdmin) return (
      <React.Fragment>
        <Ons.ListItem>
          <div className='center'>Toggle Admin</div>
          <div className='right'>
            <Ons.Switch onChange={() => {
              let xhr = new XMLHttpRequest()
              xhr.open('PUT', `/api/users/${props.row.username}`)
              xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
              xhr.send(JSON.stringify({
                field: 'isAdmin',
                value: !props.row.isAdmin
              }))
            }} checked={props.row.isAdmin} />
          </div>
        </Ons.ListItem>
        <Ons.ListItem>
          <div class='center d-flex justify-content-center'>
            <Ons.Button className='bg-danger' onClick={() => {
              props.setdData({
                title: `Delete ${props.row.fullName}`,
                content: 'Are you sure you want to delete this user?',
                btnClass: 'bg-danger',
                btnText: 'Delete',
                action: () => {
                  let xhr = new XMLHttpRequest()
                  xhr.open('DELETE', `/api/users/${props.row.username}`)
                  xhr.onreadystatechange = () => {
                    if(xhr.status === 200) props.setdShown(false)
                  }
                  xhr.send()
                }
              })
              props.setdShown(true)
            }}>Delete User</Ons.Button>
          </div>
        </Ons.ListItem>
      </React.Fragment>
    )
    return null
  }

  const [unameInvalid, setUnameInvalid] = useState('d-none')
  return (
    <Ons.Page>
      <Ons.Row>
        <Ons.Col className='pl-4'>
          <h2>Profile</h2>
        </Ons.Col>
      </Ons.Row>
      <Ons.Row className='bg-light p-4'>
        <Ons.Col>
          <div className='d-flex justify-content-center align-items-center flex-column'>
            <img src={`/api/users/${props.currentUser.username}/avatar`} className='avatar' />
            <span>{props.currentUser.fullName}</span>
          </div>
          <Form onSubmit={(e) => {
            e.preventDefault()
            let update = [
              ['username', document.getElementById('settings-username-input')],
              ['password', document.getElementById('settings-password-input')],
              ['email', document.getElementById('settings-email-input')],
              ['phoneNumber', document.getElementById('settings-phone-input')],
              ['fullName', document.getElementById('settings-fullname-input')]
            ]
            let avatar = document.getElementById('settings-avatar-input')

            let finishedAvatar = {
              get: false,
              set: v => finishedAvatar.get = v
            }
            let finishedForms = {
              get: false,
              set: v => finishedForms.get = v
            }

            if(avatar.files && avatar.files.length == 1) {
              let formData = new FormData()
              formData.set('img', avatar.files[0])
              let updateAvatar = new XMLHttpRequest()
              updateAvatar.open('POST', `/api/users/${props.currentUser.username}/avatar`)
              updateAvatar.onreadystatechange = () => {
                finishedAvatar.set(true)
              }
              updateAvatar.send(formData)
            } else finishedAvatar.set(true)

            for(var field in update) {
              if(update[field][1].value != '') {
                let updateUser = new XMLHttpRequest()
                updateUser.open('PUT', `/api/users/${props.currentUser.username}`)
                updateUser.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
                updateUser.onreadystatechange = () => {
                  if(Number(field) === 4) finishedForms.set(true)
                  if(updateUser.status === 400) setUnameInvalid('')
                }
                updateUser.send(JSON.stringify({
                  field: update[field][0],
                  value: update[field][1].value
                }))
              }
            }

            setInterval(() => {
              console.log(`${finishedAvatar.get}\n${finishedForms.get}`)
              if(finishedAvatar.get && finishedForms.get) window.open('/settings', '_self')
            }, 2000)

          }}>
            <Ons.Row>
              Username
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Input modifier='underbar' className='w-100' id='settings-username-input' type="text" placeholder={props.currentUser.username} />
              <span className={'text-warning ' + unameInvalid}>Username or email already exists</span>
            </Ons.Row>
            <Ons.Row style={{paddingTop: '1rem'}}>
              Password
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Input modifier='underbar' className='w-100' id='settings-password-input' type="password" />
            </Ons.Row>
            <Ons.Row style={{paddingTop: '1rem'}}>
              Email
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Input modifier='underbar' className='w-100' id='settings-email-input' type="email" placeholder={props.currentUser.email} />
            </Ons.Row>
            <Ons.Row style={{paddingTop: '1rem'}}>
              Phone Number
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Input modifier='underbar' className='w-100' id='settings-phone-input' type="text" placeholder={props.currentUser.phoneNumber} />
            </Ons.Row>
            <Ons.Row style={{paddingTop: '1rem'}}>
              Avatar
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Input modifier='underbar' className='w-100' id='settings-avatar-input' type="file" />
            </Ons.Row>
            <Ons.Row style={{paddingTop: '1rem'}}>
              Full Name
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Input modifier='underbar' className='w-100' id='settings-fullname-input' type="text" placeholder={props.currentUser.fullName} />
            </Ons.Row>
            <Ons.Button style={{marginTop: '1rem'}} type='submit'><Ons.Icon icon='md-save' />&nbsp;Save</Ons.Button>
          </Form>
        </Ons.Col>
      </Ons.Row>
      <CreateUser />
      <Ons.Row>
        <Ons.Col className='pl-4'>
          <h2>Users</h2>
        </Ons.Col>
      </Ons.Row>
      <Ons.Row>
        <Ons.Col className='p-2 bg-light'>
          <Ons.List 
            dataSource={props.users}
            renderRow={(row, idx) => (
              <Ons.ListItem expandable={true} tappable={true}>
                <div className="left"><img className='list-item__thumbnail' src={`/api/users/${row.username}/avatar`} /></div>
                <div className="center">
                  <span className='list-item__title'>{row.username}</span><span className='list-item__subtitle'>{row.email}</span>
                </div>
                <div className="expandable-content">
                  <Ons.List>
                    <Ons.ListItem>
                      <div className='center'>Full Name: {row.fullName}</div>
                    </Ons.ListItem>
                    <Ons.ListItem>
                      <div className='center'>Phone Number: {row.phoneNumber}</div>
                    </Ons.ListItem>
                    <Ons.ListItem>
                      <div className='center'>Admin: {String(row.isAdmin)}</div>
                    </Ons.ListItem>
                    <UserMenuButtons currentUser={props.currentUser} row={row} setdData={props.setdData} setdShown={props.setdShown} />
                  </Ons.List>
                </div>
              </Ons.ListItem>
            )}
          />
        </Ons.Col>
      </Ons.Row>
    </Ons.Page>
  )
}

export default Settings