import React, { useState, useEffect } from 'react'
import * as Ons from 'react-onsenui'
import $ from 'jquery'

const About = () => {
  const [version, setVersion] = useState('')
  const getVersion = () => {
    $.get('/api/version', (data) => {
      setVersion(data.version)
    })
  }

  useEffect(getVersion, [])

  return (
    <Ons.Page>
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '100%'}}>
          <img src='/img/Stocker-Logo-Square.svg' height='100' />
          <h1>Stocker Server</h1>
          <h3>By Just One Guy Studios</h3>
          <small className="text-muted">Version <span>{version}</span></small>
          <div style={{width: '280px', display: 'flex', justifyContent: 'space-around', padding: '0.75rem'}}>
              <a href="https://gitlab.com/just1guy/stocker"><img src='/img/gitlab.svg' height="48" /></a>
              <a href="https://mongodb.com"><img src='/img/mongodb.png' height="48" /></a>
              <a href="https://reactjs.org"><img src='/img/react.png' height="48" /></a>
              <a href="https://hapijs.com"><img src='/img/hapi.png' height="48" /></a>
          </div>
      </div>
    </Ons.Page>
  )
}

export default About
