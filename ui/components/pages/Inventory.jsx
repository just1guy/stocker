import React, { useState, useEffect, useCallback } from 'react'
import { Table, Button, Container, Row, Col, Form, FormControl, InputGroup } from 'react-bootstrap'
import ons from 'onsenui'
import * as Ons from 'react-onsenui'

const InventoryItemMenu = (props) => {
  const [range, setRange] = useState(0)
  const renderEditButtons = () => {
    if(props.currentUser.isAdmin) return (
      <Ons.Row style={{marginTop: '1rem'}}>
        <Ons.Col style={{paddingRight: '0.5rem'}}>
          <Ons.Button modifier='large' className='bg-danger' onClick={() => {
            props.setdData({
              title: `Delete ${props.row.prodName}`,
              content: 'Are you sure you want to delete this item?',
              btnText: 'Delete',
              btnClass: 'bg-danger',
              action: () => {
                let xhr = new XMLHttpRequest()
                xhr.open('DELETE', `/api/items/${props.row._id}`)
                xhr.onreadystatechange = () => {
                  if(xhr.status === 200) {
                    props.setdShown(false)
                  }
                }
                xhr.send()
              }
            })
            props.setdShown(true)
          }}>
            <Ons.Icon icon='md-delete' />&nbsp;Delete
          </Ons.Button>
        </Ons.Col>
        <Ons.Col style={{paddingLeft: '0.5rem'}}>
          <Ons.Button modifier='large' className='bg-success' onClick={() => {
            props.setcpData({
              data: props.row,
              action: 'Edit Item'
            })
            props.setcpShown(true)
          }}><Ons.Icon icon='md-edit' />&nbsp;Edit</Ons.Button>
        </Ons.Col>
      </Ons.Row>
    )
    return null
  }
  return (
    <React.Fragment>
      <Ons.List modifier='noborder' key={props.row._id}>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Brand: {props.row.brand}</div>
        </Ons.ListItem>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Category: {props.row.category}</div>
        </Ons.ListItem>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Type: {props.row.type}</div>
        </Ons.ListItem>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Model: {props.row.modelNumber}</div>
        </Ons.ListItem>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Location: {props.row.location}</div>
        </Ons.ListItem>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Storage: {props.row.storageLocation}</div>
        </Ons.ListItem>
        <Ons.ListItem modifier='longdivider'>
          <div className='center'>Notes: {props.row.notes}</div>
        </Ons.ListItem>
      </Ons.List>
      {renderEditButtons()}
      <div className='d-flex align-items-center' style={{paddingTop: '1rem'}}>
        <div className='w-75 d-flex flex-column justify-content-center align-items-center'>
          <span>{range}</span>
          <Ons.Range className='w-100' value={range} min={-props.row.quantity} max={10} onChange={(e) => setRange(Number(e.target.value))} />
        </div>
        <div className='w-25 d-flex justify-content-center align-items-center'>
          <Ons.Button onClick={() => {
            if(range === 0) return
            let xhr = new XMLHttpRequest()
            xhr.open('PUT', `/api/items/${props.row._id}`)
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
            xhr.onreadystatechange = () => {
              if(xhr.status === 200) setRange(0)
            }
            xhr.send(JSON.stringify({
              history: {
                user: props.currentUser.username,
                changed: range < 0 ? 'Removed' : 'Added',
                quantity: Math.abs(range)
              },
              quantity: props.row.quantity + range
            }))
          }}><Ons.Icon icon='md-save' /><span className='d-none d-sm-inline'>&nbsp;Save</span></Ons.Button>
        </div>
      </div>
    </React.Fragment>
  )
}

const InventoryItem = (props) => {
  const [shown, setShown] = useState(false)
  return (
    <Ons.ListItem 
      tappable={true}
      expandable={true}
      expanded={shown}
      className={props.isLow(props.row.quantity, props.row.required)}
      >
      <div className='left' onClick={() => {setShown(!shown)}}>{props.row.prodName}</div>
      <div className='d-flex center' onClick={() => {setShown(!shown)}}>
        <div className='ml-auto' style={{width: '40px'}}>{props.row.quantity}</div>
        <div style={{width: '40px'}}>{props.row.required}</div>
      </div>
      <div className='expandable-content'>
        <InventoryItemMenu 
          row={props.row}
          shown={shown}
          currentUser={props.currentUser}
          navigator={props.navigator}
          setcpData={props.setcpData}
          setcpShown={props.setcpShown}
          setdData={props.setdData}
          setdShown={props.setdShown}
          />
      </div>
    </Ons.ListItem>
  )
}

const Inventory = (props) => {
  const [sortField, setSortField] = useState('prodName')
  const [sortDesc, setSortDesc] = useState(true)
  const [filter, setFilter] = useState('')

  const makeList = () => {
    return props.items.sort((a, b) => {
      let res
      switch(sortField) {
        case 'prodName':
          res = sortDesc ? a.prodName < b.prodName : a.prodName > b.prodName
          return res ? -1 : 1
        case 'type':
          if(a.type < b.type) return sortDesc ? -1 : 1
          else if (a.type === b.type) {
            if(a.prodName < b.prodName) return sortDesc ? -1 : 1
            else return sortDesc ? 1 : -1
          }
          else return sortDesc ? 1 : -1
        case 'quantity':
          res = sortDesc ? a.quantity < b.quantity : a.quantity > b.quantity
          return res ? -1 : 1
        case 'required':
          res = sortDesc ? a.required < b.required : a.required > b.required
          return res ? -1 : 1
        case 'location':
          if(a.location < b.location) return sortDesc ? -1 : 1
          else if (a.location === b.location) {
            if(a.prodName < b.prodName) return sortDesc ? -1 : 1
            else return sortDesc ? 1 : -1
          }
          else return sortDesc ? 1 : -1
        case 'category':
          if(a.category < b.category) return sortDesc ? -1 : 1
          else if (a.category === b.category) {
            if(a.prodName < b.prodName) return sortDesc ? -1 : 1
            else return sortDesc ? 1 : -1
          }
          else return sortDesc ? 1 : -1        
        case 'low':
          if(a.quantity === 0) {
            if(b.quantity === 0) {
              if(a.required > b.required) return sortDesc ? -1 : 1
              else if (a.required === b.required) {
                if(a.prodName === b.prodName) return sortDesc ? -1 : 1
                else return sortDesc ? 1 : -1
              }
              else return sortDesc ? 1 : -1
            }
            else return sortDesc ? -1 : 1
          }
          else {
            if(b.quantity === 0) return sortDesc ? 1 : -1
            if(a.quantity < a.required) {
              if(b.quantity < b.required) {
                if(a.quantity < b.quantity) {
                  if(a.quantity - a.required < b.quantity - b.required) return sortDesc ? -1 : 1
                  else if (a.quantity - a.required === b.quantity - b.required) {
                    if(a.prodName < b.prodName) return sortDesc ? -1 : 1
                    else return sortDesc ? 1 : -1
                  }
                  else return sortDesc ? 1 : -1
                }
                else {
                  if(a.quantity - a.required < b.quantity - b.required) return sortDesc ? -1 : 1
                  else if (a.quantity - a.required === b.quantity - b.required) {
                    if(a.prodName < b.prodName) return sortDesc ? -1 : 1
                    else return sortDesc ? 1 : -1
                  }
                  else return sortDesc ? 1 : -1
                }
              }
              else return sortDesc ? -1 : 1
            } 
            else {
              if(b.quantity < b.required) return sortDesc ? 1 : -1
              else {
                if(a.quantity - a.required < b.quantity - b.required) return sortDesc ? -1 : 1
                else if (a.quantity - a.required === b.quantity - b.required) {
                  if(a.prodName < b.prodName) return sortDesc ? -1 : 1
                  else return sortDesc ? 1 : -1
                }
                else return sortDesc ? 1 : -1
              }
            }
          }
      }
    }).filter(item => {
      return JSON.stringify({
        prodName: item.prodName,
        brand: item.brand,
        category: item.category,
        modelNumber: item.modelNumber,
        quantity: item.quantity,
        required: item.required,
        type: item.type,
        location: item.location,
        storageLocation: item.storageLocation,
        UPC: item.UPC,
        specs: item.specs,
        notes: item.notes,
      }).toLowerCase().includes(filter.toLowerCase())
    })
  }

  const addItemIOS = () => {
    if(ons.platform.isIOS() && props.currentUser.isAdmin) return (
      <Ons.Row  style={{paddingTop: '1rem'}}>
        <Ons.Col>
          <Ons.Button style={{width: '100%'}} className='bg-success' onClick={() => {
            props.setcpData({
              data: undefined,
              action: 'Create Item'
            })
            props.setcpShown(true)
          }}>Add Item</Ons.Button>
        </Ons.Col>
      </Ons.Row>
    )
    return null
  }

  const addItemAndroid = () => {
    if(ons.platform.isAndroid() && props.currentUser.isAdmin) return (
      <Ons.Fab position='bottom right' ripple={true} className='bg-success text-light' onClick={() => {
        props.setcpData({
          data: undefined,
          action: 'Create Item'
        })
        props.setcpShown(true)
      }}>
        <Ons.Icon icon='md-plus' />
      </Ons.Fab>
    )
    return null
  }

  const isLow = (qty, req) => {
    if(qty <= 0) return 'item-empty'
    if(qty < req) return 'item-low'
    return 'item-full'
  }

  return (
    <React.Fragment>
      <Ons.Page>
        <div style={{width: '99vw', display: 'flex', justifyContent: 'center'}}>
          <div style={{padding: '1rem', maxWidth: '1140px', width: '100%'}}>
            <Ons.Row>
              <Ons.Col width={'100%'}>
                <Ons.SearchInput style={{width: '100%'}} placeholder="Filter" value={filter} onChange={(e) => {setFilter(e.target.value)}} />
              </Ons.Col>
            </Ons.Row>
            <Ons.Row style={{paddingTop: '1rem'}}>
              <Ons.Col className='py-2'>
                Sort By
              </Ons.Col>
            </Ons.Row>
            <Ons.Row style={{paddingTop: '0.5rem'}}>
              <Ons.Col style={{paddingRight: '0.5rem'}}>
                <Ons.Select style={{width: '100%'}} as='select' value={sortField} onChange={(e) => {setSortField(e.target.value)}}>
                  <option value='prodName'>Product</option>
                  <option value='type'>Type</option>
                  <option value='category'>Category</option>
                  <option value='quantity'>Quantity</option>
                  <option value='required'>Required</option>
                  <option value='location'>Location</option>
                  <option value='low'>Running Low</option>
                </Ons.Select>
              </Ons.Col>
              <Ons.Col style={{paddingLeft: '0.5rem'}}>
                <Ons.Button style={{width: '100%'}} onClick={() => {setSortDesc(!sortDesc)}}>{sortDesc ? 'Descending' : 'Ascending'}</Ons.Button>
              </Ons.Col>
            </Ons.Row>
            {addItemIOS()}
            <Ons.List
              className='mt-2'
              style={{marginBottom: '72px'}}
              modifier='longdivider'
              dataSource={makeList()}
              renderHeader={() => (
                <Ons.ListHeader>
                  <div className='d-flex'>
                    <div>Product</div>
                    <div className='ml-auto' style={{width: '40px'}}>Qty</div>
                    <div style={{width: '40px', marginRight: '22px'}}>Req</div>
                  </div>
                </Ons.ListHeader>
              )}
              renderRow={(row, idx) => (
                <InventoryItem 
                  key={row._id} 
                  isLow={isLow} 
                  row={row} 
                  currentUser={props.currentUser} 
                  setcpData={props.setcpData} 
                  setcpShown={props.setcpShown}
                  setdData={props.setdData}
                  setdShown={props.setdShown} />
              )}
              />
          </div>
        </div>
      </Ons.Page>
      {addItemAndroid()}
    </React.Fragment>
  )
}

export default Inventory