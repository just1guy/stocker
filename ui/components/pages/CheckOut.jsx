import React, { useState, useEffect } from 'react'
import * as Ons from 'react-onsenui'
import Instascan from 'instascan'

const CheckOut = (props) => {

  useEffect(() => {
    let scanner = new Instascan.Scanner({ video: document.getElementById('scan') })
    scanner.addListener('scan', (content) => {
      console.log(content)
    })
    Instascan.Camera.getCameras().then((cameras) => {
      if(cameras.length > 0) {
        scanner.start(cameras[0])
      } else {
        console.error('No cameras found')
      }
    }).catch((e) => {
      console.error(e)
    })
  }, [])

  return (
    <Ons.Page>
      <video id='scan'></video>
    </Ons.Page>
  )
}
export default CheckOut