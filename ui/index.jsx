import React from 'react'
import ReactDOM from 'react-dom'
// import './css/main.scss'
// import './css/bootstrap/bootstrap.scss'
// import './css/font/material-icons.css'
import App from './App'

function renderApp() {
  ReactDOM.render(<App />, document.getElementById('app'))
}

renderApp()

module.hot.accept(renderApp)
