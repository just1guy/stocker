/**
 * Cookie Password
 * Note: Please change this or set env var COOKIE_PASS to a value nobody else knows before starting server
 */
exports.cookiepass = '12345678909876543212345678909876'
/**
 * Clean up avatar directory (in milliseconds)
 */
exports.cleanUp = 60 * 60 * 1000
/**
 * Is registration enabled
 */
exports.registrationEnabled = false