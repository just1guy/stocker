//  Init servers
const Hapi = require('hapi')
const server = new Hapi.Server({
  port: process.env.PORT || 3000,
  host: process.env.SERVER_HOST || 'localhost'
})
const io = require('socket.io')(server.listener)
const CONFIG = require('./config')
const initRoutes = require('./api/routes')
//  Import authentication modules
const hAuth = require('./api/auth/hapi-validate')
const sAuth = require('./api/auth/socket-validate')
//  Import DBs and assign objects
const UserDB = require('./api/db/users')
const userdb = new UserDB()
const InventoryDB = require('./api/db/inventory')
const inventorydb = new InventoryDB()
//  Import Utilities
const fs = require('fs')
const uuid = require('uuid/v1')
const del = require('delete')
const Boom = require('boom')
const imageEdit = require('./api/image-edit')

const routes = require('./api/routes')

const start = async () => {
  io.on('connection', async (socket) => {
    setTimeout(async () => {
      socket.emit('refresh')
      socket.emit('users')
      socket.emit('history')
    }, 1500)
    console.log('a user connected')
  })

  await server.register({
    plugin: require('yar'),
    options: {
      storeBlank: true,
      cookieOptions: {
        password: process.env.COOKIE_PASS || CONFIG.cookiepass || '12345678909876543212345678909876',
        isSecure: false,
      }
    }
  })
  const scheme = (server, options) => {
    return {
      authenticate: async (request, h) => {
        let token = request.yar.get('token')
        if(!token) return Boom.unauthorized('No token in cookie')
        const { isValid, credentials, response} = await options.validate(token)
        if(isValid) return h.authenticated({credentials: credentials})
        return Boom.unauthorized('Token invalid')
      },
    }
  }
  await server.auth.scheme('token', scheme)
  await server.auth.strategy('strong', 'token', { validate: hAuth.validateToken })
  await server.auth.default('strong')
  await server.register(require('inert'))

  initRoutes(server)

  
  server.route({
    method: 'GET',
    path: '/api/history',
    handler: (request, h) => {
      return inventorydb.getHistory()
    }
  })

  server.route({
    method: 'DELETE',
    path: '/api/history',
    handler: async (request, h) => {
      if(!request.auth.credentials.isAdmin) return Boom.badRequest('Must be admin to clear history')
      await inventorydb.items.updateMany({}, { $set: { history: [] }})
      io.emit('refresh')
      io.emit('history')
      return h.response().statusCode(200)
    }
  })

  server.route({
    method: 'GET',
    path: '/api/items',
    handler: (request, h) => {
      return inventorydb.getItems(request.query.options, request.headers.sort)
    }
  })

  server.route({
    method: 'POST',
    path: '/api/items',
    options: {
      payload: {
        allow: 'application/json'
      }
    },
    handler: async (request, h) => {
      let query = await inventorydb.createItem(request.payload.item)
      if(!query) return 'Failed'
      io.emit('refresh')
      return 'Success!'
    }
  })

  server.route({
    method: 'POST',
    path: '/api/items/{itemid}',
    options: {
      payload: {
        allow: 'application/json'
      }
    },
    handler: async (request, h) => {
      let { payload } = request
      for(var entry in payload) {
        await inventorydb.updateItem(request.params.itemid, payload[entry][0], payload[entry][1])
      }
      io.emit('refresh')
      console.log('success')
      return 'success'
    }
  })

  server.route({
    method: 'PUT',
    path: '/api/items/{itemid}',
    options: {
      payload: {
        allow: 'application/json'
      }
    },
    handler: async (request, h) => {
      console.log(request.payload)
      let item = await inventorydb.items.findById(request.params.itemid)
      await item.history.push(request.payload.history)
      item.quantity = request.payload.quantity
      await item.save()
      io.emit('refresh')
      io.emit('history')
      console.log('success')
      return 'success'
    }
  })

  server.route({
    method: 'DELETE',
    path: '/api/items/{itemid}',
    options: {},
    handler: async (request, h) => {
      if(request.auth.credentials.isAdmin === false) return Boom.unauthorized('Must be an admin to delete items')
      await inventorydb.items.findByIdAndDelete(request.params.itemid)
      io.emit('refresh')
      io.emit('history')
      return 'success'
    }
  })

  server.route({
    method: 'GET',
    path: '/api/token',
    handler: (request, h) => {
      return { username: request.auth.credentials.user, fullName: request.auth.credentials.fullName }
    }
  })

  server.route({
    method: 'GET',
    path: '/api/token/new',
    handler: async (request, h) => {
      let token = await userdb.generateToken(request.auth.credentials.name)
      return h.response(token).state('token', {token: token, path: '/'})
    }
  })

  server.route({
    method: 'GET',
    path: '/api/users',
    handler: async (request, h) => {
      return userdb.getUsers(request.query.options, request.query.sort)
    }
  })

  server.route({
    method: 'PUT',
    path: '/api/register',
    options: {
      auth: false
    },
    handler: async (request, h)=>{
      if(CONFIG.registrationEnabled) return 'Registration is currently disabled'
    }
  })

  server.route({
    method: 'GET',
    path: '/api/users/{username}',
    handler: (request, h) => {
      if(request.params.username === 'current') return userdb.getUser(request.auth.credentials.user, request.query.format)
      return userdb.getUser(request.params.username, request.query.format)
    }
  })

  server.route({
    method: 'PUT',
    path: '/api/users/{username}',
    options: {
      payload: {
        allow: 'application/json'
      }
    },
    handler: async (request, h) => {
      let username = request.params.username
      let field = request.payload.field
      let value = request.payload.value
      if(request.auth.credentials.user != username) {
        if(!request.auth.credentials.isAdmin) return Boom.badRequest('Must be an admin or logged in as this user to update their fields')
      }
      if(!request.auth.credentials.isAdmin && field == 'isAdmin') return Boom.badRequest('Must be an admin to set a user\'s admin field')
      if(field === 'username' || field === 'email') {
        let check = await userdb.users.find({ $or: [ { username: value }, { email: value } ] })
        if(check.length >= 1) return Boom.badRequest('Username or email already exists')
      }

      let action = userdb.updateUserFields(username, field, value)
      if(action) {
        io.emit('users')
        return h.response(200)
      }
      return 'Update failed'
    }
  })

  server.route({
    method: 'GET',
    path: '/api/users/{username}/avatar',
    handler: async (request, h) => {
      let user = await userdb.getUser(request.params.username)
      try {
        let check = await fs.existsSync(`./img/avatars/${user.avatar}`)
        if( check ) return h.file(`./img/avatars/${user.avatar}`)
        return h.file('./img/avatars/default.png')
      } catch(err) {
        return h.file('./img/avatars/default.png')
      }
    }
  })

  server.route({
    method: 'POST',
    path: '/api/users/{username}/avatar',
    handler: async (request, h) => {
      let checker = false
      if(userdb.isRequestedUser(request.auth.credentials.token, request.params.username)) checker = true
      if(userdb.isAdmin(request.auth.credentials.token)) checker = true
      if(!checker) return 'You must be an admin or logged in as the user who\'s avatar you\'re trying to change'
      let imagename = uuid()
      let data = request.payload
      let avatar = await imageEdit.makeAvatar(data.img)
      const write = await fs.writeFile(`./img/avatars/${imagename}.png`, avatar, err=>{if(err) return err})
      if (write) return Boom.badData('Upload failed')
      let user = await userdb.getUser(request.params.username)
      await del(`./img/avatars/${user.avatar}`)
      user.avatar = `${imagename}.png`
      await user.save()

      return h.response('success').type('text/plain')
    }
  })

  server.route({
    method: 'POST',
    path: '/api/users',
    options: {
      payload: {
        allow: 'application/json'
      }
    },
    handler: async(request, h) => {
      if(!request.auth.credentials.isAdmin) return Boom.badRequest('Must be admin to create a user')

      let { payload } = request
      let check = await userdb.users.find({ $or: [{username: payload.username}, {email: payload.email}]})
      console.log('checking user')
      if(check.length > 1) Boom.badRequest('Username or email already taken')
      console.log('Creating user')
      return userdb.createUser(
        payload.username,
        payload.password,
        payload.email,
        payload.phonenumber,
        payload.fullname,
        payload.isadmin)
      
    }
  })

  server.route({
    method: 'DELETE',
    path: '/api/users',
    options: {
      payload: {
        allow: 'application/json'
      }
    },
    handler: async (request, h) => {
      if(request.auth.credentials.isAdmin) {
        await userdb.users.findByIdAndDelete(request.payload.id)
        io.emit('users', await userdb.getUsers())
        console.log('deleted user')
        return 'success'
      }
      return Boom.badRequest('Must be admin to delete users')
    }
  })

  server.route({
    method: 'GET',
    path: '/img/{param*}',
    handler: {
      directory: {
        path: './img',
      }
    }
  })
  await server.start()
  console.log(`Server started at ${server.info.uri}`)
  const cleanup = async () => {
    let fileList
    await fs.readdir('./img/avatars', async (err, files) => { fileList = files })
    const avatars = await userdb.users.find({}, 'avatar', (err, res)=>res.avatar)
    let isAvatar = {
      get: false,
      set: v => isAvatar.get = v
    }
    try {
      for (var i = 0; i < fileList.length; i++) {
        for (var z = 0; z < avatars.length; z++) {
          if (fileList[i] === avatars[z].avatar || fileList[i] === 'default.png') {
            isAvatar.set(true)
          }
        }
        if (!isAvatar.get) {
          del(`./img/avatars/${fileList[i]}`)
        }
        isAvatar.set(false)
      }
    } catch(err) {
      console.log('Avatars folder does not exist yet. No cleanup necessary.')
    }
  }
  cleanup()
  setInterval(cleanup, process.env.CLEANUP_INTERVAL || CONFIG.cleanUp || 60 * 60 * 1000)
}

start()
